FROM nginx:latest

ENV DEBIAN_FRONTEND=noninteractive
RUN apt update && apt install -y vim && apt install -y mcedit && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/share/nginx/html
EXPOSE 8080

COPY index.html .
